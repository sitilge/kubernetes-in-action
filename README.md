# kubernetes-in-action

These are some quotes from the book "Kubernetes in Action" (https://www.manning.com/books/kubernetes-in-action). Some parts may be left out for clarity, or some quotes might be slightly modified. This is just for educational purposes, I do not own most of the content here.

## Chapter 1 - Introducing Kubernetes
> By this point, you’re probably wondering how exactly containers can isolate processes if they’re running on the same operating system. Two mechanisms make this possible. The first one, Linux Namespaces, makes sure each process sees its own personal view of the system (files, processes, network interfaces, hostname, and so on). The second one is Linux Control Groups (cgroups), which limit the amount of resources the process can consume (CPU, memory, network bandwidth, and so on).

> Docker images are composed of layers. Different images can contain the exact same layers because every Docker image is built on top of another image and two different images can both use the same parent image as their base. This speeds up the distribution of images across the network, because layers that have already been transferred as part of the first image don’t need to be transferred again when transferring the other image.

> But layers don’t only make distribution more efficient, they also help reduce the storage footprint of images. Each layer is only stored once. Two containers created from two images based on the same base layers can therefore read the same files, but if one of them writes over those files, the other one doesn’t see those changes. Therefore, even if they share files, they’re still isolated from each other. This works because container image layers are read-only. When a container is run, a new writable layer is created on top of the layers in the image. When the process in the container writes to a file located in one of the underlying layers, a copy of the whole file is created in the top-most layer and the process writes to the copy.

> It should also be clear that a containerized app built for a specific hardware architecture can only run on other machines that have the same architecture. You can’t containerize an application built for the x86 architecture and expect it to run on an ARM-based machine because it also runs Docker. You still need a VM for that.

> To run an application in Kubernetes, you first need to package it up into one or more container images, push those images to an image registry, and then post a description of your app to the Kubernetes API server.

> Certain cases do exist where the developer does care what kind of hardware the application should run on. If the nodes are heterogeneous, you’ll find cases when you want certain apps to run on nodes with certain capabilities and run other apps on others. For example, one of your apps may require being run on a system with SSDs instead of HDDs, while other apps run fine on HDDs. In such cases, you obviously want to ensure that particular app is always scheduled to a node with an SSD.

## Chapter 3 - Pods: running containers in Kubernetes

> All pods in a Kubernetes cluster reside in a single flat, shared, network-address space, which means every pod can access every other pod at the other pod’s IP address. No NAT (Network Address Translation) gateways exist between them.

> When two pods send network packets between each other, they’ll each see the actual IP address of the other as the source IP in the packet.

> To recap how containers should be grouped into pods—when deciding whether to put two containers into a single pod or into two separate pods, you always need to ask yourself the following questions: Do they need to be run together or can they run on different hosts? Do they represent a single whole or are they independent components? Must they be scaled together or individually?

> Basically, you should always gravitate toward running containers in separate pods, unless a specific reason requires them to be part of the same pod.

> Specifying ports in the pod definition is purely informational. Omitting them has no effect on whether clients can connect to the pod through the port or not. If the container is accepting connections through a port bound to the 0.0.0.0 address, other pods can always connect to it, even if the port isn’t listed in the pod spec explicitly.

> But it makes sense to define the ports explicitly so that everyone using your cluster can quickly see what ports each pod exposes. Explicitly defining ports also allows you to assign a name to each port, which can come in handy, as you’ll see later in the book.

> A canary release is when you deploy a new version of an application next to the stable version, and only let a small fraction of users hit the new version to see how it behaves before rolling it out to all users. This prevents bad releases from being exposed to too many users.

> The kubectl get pods command doesn’t list any labels by default, but you can see them by using the --show-labels switch:

> You’ve added a nodeSelector field under the spec section. When you create the pod, the scheduler will only choose among the nodes that contain the label.

> Using multiple namespaces allows you to split complex systems with numerous components into smaller distinct groups. They can also be used for separating resources in a multi-tenant environment, splitting up resources into production, development, and QA environments, or in any other way you may need. Resource names only need to be unique within a namespace. Two different namespaces can contain resources of the same name. But, while most types of resources are namespaced, a few aren’t. One of them is the Node resource, which is global and not tied to a single namespace.

> You may think that when different users deploy pods across different namespaces, those pods are isolated from each other and can’t communicate, but that’s not necessarily the case. Whether namespaces provide network isolation depends on which networking solution is deployed with Kubernetes. When the solution doesn’t provide inter-namespace network isolation, if a pod in namespace foo knows the IP address of a pod in namespace bar, there is nothing preventing it from sending traffic, such as HTTP requests, to the other pod.

## Chapter 4 - Replication and other controllers: deploying managed pods

> As soon as a pod is scheduled to a node, the Kubelet on that node will run its containers and, from then on, keep them running as long as the pod exists. If the container’s main process crashes, the Kubelet will restart the container. If your application has a bug that causes it to crash every once in a while, Kubernetes will restart it automatically, so even without doing anything special in the app itself, running the app in Kubernetes automatically gives it the ability to heal itself.

> When you want to figure out why the previous container terminated, you’ll want to see those logs instead of the current container’s logs. This can be done by using the --previous option: kubectl logs mypod --previous,

> If you don’t set the initial delay, the prober will start probing the container as soon as it starts, which usually leads to the probe failing, because the app isn’t ready to start receiving requests. If the number of failures exceeds the failure threshold, the container is restarted before it’s even able to start responding to requests properly.

> Always remember to set an initial delay to account for your app’s startup time.

> Your simplistic liveness probe simply checks if the server is responding. While this may seem overly simple, even a liveness probe like this does wonders, because it causes the container to be restarted if the web server running within the container stops responding to HTTP requests. Compared to having no liveness probe, this is a major improvement, and may be sufficient in most cases.

> A ReplicationController constantly monitors the list of running pods and makes sure the actual number of pods of a “type” always matches the desired number. If too few such pods are running, it creates new replicas from a pod template. If too many such pods are running, it removes the excess replicas.

> A pod instance is never relocated to another node. Instead, the ReplicationController creates a completely new pod instance that has no relation to the instance it’s replacing.

> Although a pod isn’t tied to a ReplicationController, the pod does reference it in the metadata.ownerReferences field, which you can use to easily find which ReplicationController a pod belongs to.

> When deleting a ReplicationController with kubectl delete, you can keep its pods running by passing the --cascade=false option to the command.

> Initially, ReplicationControllers were the only Kubernetes component for replicating pods and rescheduling them when nodes failed. Later, a similar resource called a ReplicaSet was introduced. It’s a new generation of ReplicationController and replaces it completely (ReplicationControllers will eventually be deprecated).

> A ReplicaSet behaves exactly like a ReplicationController, but it has more expressive pod selectors. Whereas a ReplicationController label selector only allows matching pods that include a certain label, a ReplicaSet selector also allows matching pods that lack a certain label or pods that include a certain label key, regardless of its value.

> To run a pod on all cluster nodes, you create a DaemonSet object, which is much like a ReplicationController or a ReplicaSet, except that pods created by a DaemonSet already have a target node specified and skip the Kubernetes Scheduler. They aren’t scattered around the cluster randomly.

> Kubernetes includes support for this through the Job resource, which is similar to the other resources we’ve discussed in this chapter, but it allows you to run a pod whose container isn’t restarted when the process running inside finishes successfully. Once it does, the pod is considered complete.

> For example, Jobs are useful for ad hoc tasks, where it’s crucial that the task finishes properly. You could run the task in an unmanaged pod and wait for it to finish, but in the event of a node failing or the pod being evicted from the node while it is performing its task, you’d need to manually recreate it. Doing this manually doesn’t make sense—especially if the job takes hours to complete.

> After the two minutes have passed, the pod will no longer show up in the pod list and the Job will be marked as completed. By default, completed pods aren’t shown when you list pods, unless you use the --show-all switch.

> Jobs may be configured to create more than one pod instance and run them in parallel or sequentially. This is done by setting the completions and the parallelism properties in the Job spec.

> If you need a Job to run more than once, you set completions to how many times you want the Job’s pod to run. The following listing shows an example.

> Instead of running single Job pods one after the other, you can also make the Job run multiple pods in parallel.

> We need to discuss one final thing about Jobs. How long should the Job wait for a pod to finish? What if the pod gets stuck and can’t finish at all (or it can’t finish fast enough)? A pod’s time can be limited by setting the activeDeadlineSeconds property in the pod spec. If the pod runs longer than that, the system will try to terminate it and will mark the Job as failed.

## Chapter 5 - Services: enabling clients to discover and talk to pods

> A Kubernetes Service is a resource you create to make a single, constant point of entry to a group of pods providing the same service. Each service has an IP address and port that never change while the service exists. Clients can open connections to that IP and port, and those connections are then routed to one of the pods backing that service. This way, clients of a service don’t need to know the location of individual pods providing the service, allowing those pods to be moved around the cluster at any time.

> Additionally, by creating the service, you also enable the frontend pods to easily find the backend service by its name through either environment variables or DNS.

> If you execute the same command a few more times, you should hit a different pod with every invocation, because the service proxy normally forwards each connection to a randomly selected backing pod, even if the connections are coming from the same client. If, on the other hand, you want all requests made by a certain client to be redirected to the same pod every time, you can set the service’s sessionAffinity property to ClientIP (instead of None, which is the default).

> Your service exposes only a single port, but services can also support multiple ports. For example, if your pods listened on two ports—let’s say 8080 for HTTP and 8443 for HTTPS—you could use a single service to forward both port 80 and 443 to the pod’s ports 8080 and 8443. You don’t need to create two different services in such cases. Using a single, multi-port service exposes all the service’s ports through a single cluster IP.

> When creating a service with multiple ports, you must specify a name for each port.

> But why should you even bother with naming ports? The biggest benefit of doing so is that it enables you to change port numbers later without having to change the service spec.

> But how do the client pods know the IP and port of a service? Do you need to create the service first, then manually look up its IP address and pass the IP to the configuration options of the client pod? Not really. Kubernetes also provides ways for client pods to discover a service’s IP and port.

> You’ll see the *_SERVICE_HOST and the *_SERVICE_PORT environment variables, which hold the IP address and port of the respective services.

> Environment variables are one way of looking up the IP and port of a service, but isn’t this usually the domain of DNS? Why doesn’t Kubernetes include a DNS server and allow you to look up service IPs through DNS instead? As it turns out, it does! You can simply access the service by it's name!

> Hmm. curl-ing the service works, but pinging it doesn’t. That’s because the service’s cluster IP is a virtual IP, and only has meaning when combined with the service port.

> Services don’t link to pods directly. Instead, a resource sits in between—the Endpoints resource. You may have already noticed endpoints if you used the kubectl describe command on your service, as shown in the following listing.

> Although the pod selector is defined in the service spec, it’s not used directly when redirecting incoming connections. Instead, the selector is used to build a list of IPs and ports, which is then stored in the Endpoints resource. When a client connects to a service, the service proxy selects one of those IP and port pairs and redirects the incoming connection to the server listening at that location.

> Endpoints are a separate resource and not an attribute of a service. Because you created the service without a selector, the corresponding Endpoints resource hasn’t been created automatically, so it’s up to you to create it.

> You have a few ways to make a service accessible externally: 1) Setting the service type to NodePort—For a NodePort service, each cluster node opens a port on the node itself (hence the name) and redirects traffic received on that port to the underlying service. The service isn’t accessible only at the internal cluster IP and port, but also through a dedicated port on all nodes. 2) Setting the service type to LoadBalancer, an extension of the NodePort type—This makes the service accessible through a dedicated load balancer, provisioned from the cloud infrastructure Kubernetes is running on. The load balancer redirects traffic to the node port across all the nodes. Clients connect to the service through the load balancer’s IP. 3) Creating an Ingress resource, a radically different mechanism for exposing multiple services through a single IP address—It operates at the HTTP level (network layer 7) and can thus offer more features than layer 4 services can.

> Because your service is now exposed externally, you may try accessing it with your web browser. You’ll see something that may strike you as odd—the browser will hit the exact same pod every time. Did the service’s session affinity change in the meantime? With kubectl explain, you can double-check that the service’s session affinity is still set to None, so why don’t different browser requests hit different pods, as is the case when using curl?
Let me explain what’s happening. The browser is using keep-alive connections and sends all its requests through a single connection, whereas curl opens a new connection every time. Services work at the connection level, so when a connection to a service is first opened, a random pod is selected and then all network packets belonging to that connection are all sent to that single pod. Even if session affinity is set to None, users will always hit the same pod (until the connection is closed).

> ExternalName services are implemented solely at the DNS level—a simple CNAME DNS record is created for the service. Therefore, clients connecting to the service will connect to the external service directly, bypassing the service proxy completely. For this reason, these types of services don’t even get a cluster IP.

> That’s why it makes sense to put a load balancer in front of the nodes to make sure you’re spreading requests across all healthy nodes and never sending them to a node that’s offline at that moment.

> As already mentioned, a LoadBalancer-type service is a NodePort service with an additional infrastructure-provided load balancer. If you use kubectl describe to display additional info about the service, you’ll see that a node port has been selected for the service. If you were to open the firewall for this port, the way you did in the previous section about NodePort services, you could access the service through the node IPs as well.

> Ingress controllers on cloud providers (in GKE, for example) require the Ingress to point to a NodePort service. But that’s not a requirement of Kubernetes itself.

> When a client opens a TLS connection to an Ingress controller, the controller terminates the TLS connection. The communication between the client and the controller is encrypted, whereas the communication between the controller and the backend pod isn’t. The application running in the pod doesn’t need to support TLS. For exam-ple, if the pod runs a web server, it can accept only HTTP traffic and let the Ingress controller take care of everything related to TLS.

> The readiness probe is invoked periodically and determines whether the specific pod should receive client requests or not. When a container’s readiness probe returns success, it’s signaling that the container is ready to accept requests.

> This notion of being ready is obviously something that’s specific to each container. Kubernetes can merely check if the app running in the container responds to a simple GET / request or it can hit a specific URL path, which causes the app to perform a whole list of checks to determine if it’s ready. Such a detailed readiness probe, which takes the app’s specifics into account, is the app developer’s responsibility.

> Unlike liveness probes, if a container fails the readiness check, it won’t be killed or restarted. This is an important distinction between liveness and readiness probes. Liveness probes keep pods healthy by killing off unhealthy containers and replacing them with new, healthy ones, whereas readiness probes make sure that only pods that are ready to serve requests receive them. This is mostly necessary during container start up, but it’s also useful after the container has been running for a while.

> In the real world, the readiness probe should return success or failure depending on whether the app can (and wants to) receive client requests or not.

> You should always define a readiness probe, even if it’s as simple as sending an HTTP request to the base URL.

> When a pod is being shut down, the app running in it usually stops accepting connections as soon as it receives the termination signal. Because of this, you might think you need to make your readiness probe start failing as soon as the shutdown procedure is initiated, ensuring the pod is removed from all services it’s part of. But that’s not necessary, because Kubernetes removes the pod from all services as soon as you delete the pod.

## Chapter 6 - Volumes: attaching disk storage to containers

> We’ve said that pods are similar to logical hosts where processes running inside them share resources such as CPU, RAM, network interfaces, and others. One would expect the processes to also share disks, but that’s not the case. You’ll remember that each container in a pod has its own isolated filesystem, because the file-system comes from the container’s image.

> Here’s a list of several of the available volume types:
 - emptyDir - A simple empty directory used for storing transient data.
 - hostPath - Used for mounting directories from the worker node’s filesystem into the pod.
 - gitRepo - A volume initialized by checking out the contents of a Git repository.
 - nfs - An NFS share mounted into the pod.
 - gcePersistentDisk, awsElastic-BlockStore (Amazon Web Services Elastic Block Store Volume), azureDisk (Microsoft Azure Disk Volume) - Used for mounting cloud provider-specific storage.
 - cinder, cephfs, iscsi, flocker, glusterfs, quobyte, rbd, flexVolume, vsphere-Volume, photonPersistentDisk, scaleIO - Used for mounting other types of network storage. 
 - configMap, secret, downwardAPI - Special types of volumes used to expose certain Kubernetes resources and cluster information to the pod.
 - persistentVolumeClaim - A way to use a pre- or dynamically provisioned persistent storage.

> Most pods should be oblivious of their host node, so they shouldn’t access any files on the node’s filesystem. But certain system-level pods (remember, these will usually be managed by a DaemonSet) do need to either read the node’s files or use the node’s filesystem to access the node’s devices through the filesystem. Kubernetes makes this possible through a hostPath volume.

> Remember to use hostPath volumes only if you need to read or write system files on the node. Never use them to persist data across pods.

> Ideally, a developer deploying their apps on Kubernetes should never have to know what kind of storage technology is used underneath, the same way they don’t have to know what type of physical servers are being used to run their pods. Infrastructure-related dealings should be the sole domain of the cluster administrator.

> To enable apps to request storage in a Kubernetes cluster without having to deal with infrastructure specifics, two new resources were introduced. They are Persistent-Volumes and PersistentVolumeClaims. The names may be a bit misleading, because as you’ve seen in the previous few sections, even regular Kubernetes volumes can be used to store persistent data.

> Instead of the developer adding a technology-specific volume to their pod, it’s the cluster administrator who sets up the underlying storage and then registers it in Kubernetes by creating a PersistentVolume resource through the Kubernetes API server. When creating the PersistentVolume, the admin specifies its size and the access modes it supports.

> When a cluster user needs to use persistent storage in one of their pods, they first create a PersistentVolumeClaim manifest, specifying the minimum size and the access mode they require. The user then submits the PersistentVolumeClaim manifest to the Kubernetes API server, and Kubernetes finds the appropriate PersistentVolume and binds the volume to the claim. The PersistentVolumeClaim can then be used as one of the volumes inside a pod.

> Other users cannot use the same PersistentVolume until it has been released by deleting the bound PersistentVolumeClaim.

> Claiming a PersistentVolume is a completely separate process from creating a pod, because you want the same PersistentVolumeClaim to stay available even if the pod is rescheduled (remember, rescheduling means the previous pod is deleted and a new one is created).

> As soon as you create the claim, Kubernetes finds the appropriate PersistentVolume and binds it to the claim. The PersistentVolume’s capacity must be large enough to accommodate what the claim requests. Additionally, the volume’s access modes must include the access modes requested by the claim.

> PersistentVolumes don’t belong to any namespace. They’re cluster-level resources like nodes.

> RWO, ROX, and RWX pertain to the number of worker nodes that can use the volume at the same time, not to the number of pods!

> Additionally, the same pod and claim manifests can now be used on many different Kubernetes clusters, because they don’t refer to anything infrastructure-specific. The claim states, “I need x amount of storage and I need to be able to read and write to it by a single client at once,” and then the pod references the claim by name in one of its volumes.

> You’ve seen how using PersistentVolumes and PersistentVolumeClaims makes it easy to obtain persistent storage without the developer having to deal with the actual storage technology used underneath. But this still requires a cluster administrator to provision the actual storage up front. Luckily, Kubernetes can also perform this job automatically through dynamic provisioning of PersistentVolumes.

> The cluster admin, instead of creating PersistentVolumes, can deploy a Persistent-Volume provisioner and define one or more StorageClass objects to let users choose what type of PersistentVolume they want. The users can refer to the StorageClass in their PersistentVolumeClaims and the provisioner will take that into account when provisioning the persistent storage.

> Similar to PersistentVolumes, StorageClass resources aren’t namespaced.

> The nice thing about StorageClasses is the fact that claims refer to them by name. The PVC definitions are therefore portable across different clusters, as long as the StorageClass names are the same across all of them.

> The default storage class is what’s used to dynamically provision a PersistentVolume if the PersistentVolumeClaim doesn’t explicitly say which storage class to use.

## Chapter 7 - ConfigMaps and Secrets configuring applications

> Regardless if you’re using a ConfigMap to store configuration data or not, you can configure your apps by:
 - Passing command-line arguments to containers
 - Setting custom environment variables for each container
 - Mounting configuration files into containers through a special type of volume

> The first thing I need to explain is that the whole command that gets executed in the container is composed of two parts: the command and the arguments.

> UNDERSTANDING ENTRYPOINT AND CMD In a Dockerfile, two instructions define the two parts:
 - ENTRYPOINT defines the executable invoked when the container is started.
 - CMD specifies the arguments that get passed to the ENTRYPOINT. Although you can use the CMD instruction to specify the command you want to execute when the image is run, the correct way is to do it through the ENTRYPOINT instruction and to only specify the CMD if you want to define the default arguments.

> The shell process is unnecessary, which is why you should always use the exec form of the ENTRYPOINT instruction.

> In Kubernetes, when specifying a container, you can choose to override both ENTRYPOINT and CMD. To do that, you set the properties command and args in the container specification.

> Although it would be useful to also define environment variables at the pod level and have them be inherited by its containers, no such option currently exists.

> As mentioned previously, you set the environment variable inside the container definition, not at the pod level.

> In the previous example, you set a fixed value for the environment variable, but you can also reference previously defined environment variables or any other existing variables by using the $(VAR) syntax.

> An application doesn’t need to read the ConfigMap directly or even know that it exists. The contents of the map are instead passed to containers as either environment variables or as files in a volume

> Regardless of how an app consumes a ConfigMap, having the config in a separate standalone object like this allows you to keep multiple manifests for ConfigMaps with the same name, each for a different environment (development, testing, QA, production, and so on). Because pods reference the ConfigMap by name, you can use a different config in each environment while using the same pod specification across all of them.

> Instead of importing each file individually, you can even import all files from a file directory:
$ kubectl create configmap my-config --from-file=/path/to/dir . In this case, kubectl will create an individual map entry for each file in the specified directory, but only for files whose name is a valid ConfigMap key.

> How do you now get the values from this map into a pod’s container? You have three options. Let’s start with the simplest—setting an environment variable.

> You might wonder what happens if the referenced ConfigMap doesn’t exist when you create the pod. Kubernetes schedules the pod normally and tries to run its containers. The container referencing the non-existing ConfigMap will fail to start, but the other container will start normally. If you then create the missing ConfigMap, the failed container is started without requiring you to recreate the pod.

> You can also mark a reference to a ConfigMap as optional (by setting configMapKeyRef.optional: true). In that case, the container starts even if the ConfigMap doesn’t exist.

> When your ConfigMap contains more than just a few entries, it becomes tedious and error-prone to create environment variables from each entry individually. Luckily, Kubernetes version 1.6 provides a way to expose all entries of a ConfigMap as environment variables.

> Imagine having a ConfigMap with three keys called FOO, BAR, and FOO-BAR. You can expose them all as environment variables by using the envFrom attribute, instead of env the way you did in previous examples.

> Now, let’s also look at how to pass values from a ConfigMap as arguments to the main process running in the container. You can’t reference ConfigMap entries directly in the pod.spec.containers.args field, but you can first initialize an environment variable from the ConfigMap entry and then refer to the variable inside the arguments.

> Creating a volume populated with the contents of a ConfigMap is as easy as creating a volume that references the ConfigMap by name and mounting the volume in a container. You already learned how to create volumes and mount them, so the only thing left to learn is how to initialize the volume with files created from a Config-Map’s entries.

> Luckily, you can populate a configMap volume with only part of the ConfigMap’s entries. To define which entries should be exposed as files in a configMap volume, use the volume’s items attribute.

> There’s one important thing to discuss at this point. In both this and in your previous example, you mounted the volume as a directory, which means you’ve hidden any files that are stored in the /etc/nginx/conf.d directory in the container image itself.
This is generally what happens in Linux when you mount a filesystem into a non-empty directory. The directory then only contains the files from the mounted filesystem, whereas the original files in that directory are inaccessible for as long as the filesystem is mounted.

> Naturally, you’re now wondering how to add individual files from a ConfigMap into an existing directory without hiding existing files stored in it. An additional subPath property on the volumeMount allows you to mount either a single file or a single directory from the volume instead of mounting the whole volume.

> The subPath property can be used when mounting any kind of volume. Instead of mounting the whole volume, you can mount part of it. But this method of mounting individual files has a relatively big deficiency related to updating files.

> By default, the permissions on all files in a configMap volume are set to 644 (-rw-r—r--). You can change this by setting the defaultMode property in the volume spec.

> When you update a ConfigMap, the files in all the volumes referencing it are updated. It’s then up to the process to detect that they’ve been changed and reload them. But Kubernetes will most likely eventually also support sending a signal to the container after updating the files.

> You may wonder what happens if an app can detect config file changes on its own and reloads them before Kubernetes has finished updating all the files in the configMap volume. Luckily, this can’t happen, because all the files are updated atomically, which means all updates occur at once. Kubernetes achieves this by using symbolic links. If you list all the files in the mounted configMap volume, you’ll see it.

> One big caveat relates to updating ConfigMap-backed volumes. If you’ve mounted a single file in the container instead of the whole volume, the file will not be updated!

> Kubernetes provides a separate object called a Secret. Secrets are much like ConfigMaps—they’re also maps that hold key-value pairs. They can be used the same way as a ConfigMap. You can: 
 - Pass Secret entries to the container as environment variables
 - Expose Secret entries as files in a volume

> Every pod has a secret volume attached to it automatically. The volume in the previous kubectl describe output refers to a Secret called default-token-cfee9. Because Secrets are resources, you can list them with kubectl get secrets and find the default-token Secret in that list.

> Because not all sensitive data is in binary form, Kubernetes also allows setting a Secret’s values through the stringData field.

> When you expose the Secret to a container through a secret volume, the value of the Secret entry is decoded and written to the file in its actual form (regardless if it’s plain text or binary). The same is also true when exposing the Secret entry through an environment variable. In both cases, the app doesn’t need to decode it, but can read the file’s contents or look up the environment variable value and use it directly.

> Instead of using a volume, you could also have exposed individual entries from the secret as environment variables.

> Think twice before using environment variables to pass your Secrets to your container, because they may get exposed inadvertently. To be safe, always use secret volumes for exposing Secrets.

> Given that people usually run many different pods in their systems, it makes you wonder if you need to add the same image pull Secrets to every pod. Luckily, that’s not the case. You’ll learn how image pull Secrets can be added to all your pods automatically if you add the Secrets to a ServiceAccount.

## Chapter 8 - Accessing pod metadata and other resources from applications

> Kubernetes Downward API allows you to pass metadata about the pod and its environment through environment variables or files (in a downwardAPI volume). Don’t be confused by the name. The Downward API isn’t like a REST endpoint that your app needs to hit so it can get the data. It’s a way of having environment variables or files populated with values from the pod’s specification or status.

> The Downward API enables you to expose the pod’s own metadata to the processes running inside that pod. Currently, it allows you to pass the following information to your containers:
 - The namespace the pod belongs to
 - The name of the node the pod is running on
 - The name of the service account the pod is running under 
 - The CPU and memory requests for each container 
 - The CPU and memory limits for each container 
 - The pod’s labels
 - The pod’s annotations

> Using the Downward API isn’t complicated. It allows you to keep the application Kubernetes-agnostic. This is especially useful when you’re dealing with an existing application that expects certain data in environment variables. The Downward API allows you to expose the data to the application without having to rewrite the application or wrap it in a shell script, which collects the data and then exposes it through environment variables.

> But the metadata available through the Downward API is fairly limited. If you need more, you’ll need to obtain it from the Kubernetes API server directly. You’ll learn how to do that next.

> The kubectl proxy command runs a proxy server that accepts HTTP connections on your local machine and proxies them to the API server while taking care of authentication, so you don’t need to pass the authentication token in every request. It also makes sure you’re talking to the actual API server and not a man in the middle (by verifying the server’s certificate on each request).

> You don’t need to pass in any other arguments, because kubectl already knows everything it needs (the API server URL, authorization token, and so on). As soon as it starts up, the proxy starts accepting connections on local port 8001.

> You’ve seen that you can browse the Kubernetes REST API server without using any special tools, but to fully explore the REST API and interact with it, a better option is described at the end of this chapter. For now, exploring it with curl like this is enough to make you understand how an application running in a pod talks to Kubernetes.

> First, you need to find the IP and port of the Kubernetes API server. This is easy, because a Service called kubernetes is automatically exposed in the default namespace and configured to point to the API server. You may remember seeing it every time you listed services with kubectl get svc.

> Never skip checking the server’s certificate in an actual application. Doing so could make your app expose its authentication token to an attacker using a man-in-the-middle attack.

> If you’re using a Kubernetes cluster with RBAC enabled, the service account may not be authorized to access (parts of) the API server. You’ll learn about service accounts and RBAC in chapter 12.

> export CURL_CA_BUNDLE=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
  TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
  curl -H "Authorization: Bearer $TOKEN" https://kubernetes

> Let’s recap how an app running inside a pod can access the Kubernetes API properly:
 - The app should verify whether the API server’s certificate is signed by the certificate authority, whose certificate is in the ca.crt file.
 - The app should authenticate itself by sending the Authorization header with the bearer token from the token file.
 - The namespace file should be used to pass the namespace to the API server when performing CRUD operations on API objects inside the pod’s namespace.

> Imagine having an application that (among other things) needs to query the API server. Instead of it talking to the API server directly, as you did in the previous section, you can run kubectl proxy in an ambassador container alongside the main container and communicate with the API server through it.

> Your pod now has two containers, and you want to run bash in the main container, hence the -c main option. You don’t need to specify the container explicitly if you want to run the command in the pod’s first container. But if you want to run a command inside any other container, you do need to specify the container’s name using the -c option.

## Chapter 9 - Deployments: updating applications declaratively

>  Kubernetes helps you move toward a true zero-downtime update process. Although this can be achieved using only ReplicationControllers or ReplicaSets, Kubernetes also provides a Deployment resource that sits on top of ReplicaSets and enables declarative application updates. If you’re not completely sure what that means, keep reading—it’s not as complicated as it sounds.

> Start new ones and, once they’re up, delete the old ones. You can do this either by adding all the new pods and then deleting all the old ones at once, or sequentially, by adding new pods and removing old ones gradually.
Both these strategies have their benefits and drawbacks. The first option would lead to a short period of time when your application is unavailable.

> Pods are usually fronted by a Service. It’s possible to have the Service front only the initial version of the pods while you bring up the pods running the new version. Then, once all the new pods are up, you can change the Service’s label selector and have the Service switch over to the new pods. This is called a blue-green deployment. After switching over, and once you’re sure the new version functions correctly, you’re free to delete the old pods by deleting the old ReplicationController.

> Instead of bringing up all the new pods and deleting the old pods at once, you can also perform a rolling update, which replaces pods step by step. You do this by slowly scaling down the previous ReplicationController and scaling up the new one. In this case, you’ll want the Service’s pod selector to include both the old and the new pods, so it directs requests toward both sets of pods.

> Using kubectl to perform the update makes the process much easier, but, as you’ll see later, this is now an outdated way of updating apps.

> You need to be aware that the default imagePullPolicy depends on the image tag. If a container refers to the latest tag (either explicitly or by not specifying the tag at all), imagePullPolicy defaults to Always, but if the container refers to any other tag, the policy defaults to IfNotPresent.

> To perform the update, you’ll run the kubectl rolling-update command.

> At the beginning of this section, I mentioned an even better way of doing updates than through kubectl rolling-update. What’s so wrong with this process that a bet-ter one had to be introduced? Well, for starters, I, for one, don’t like Kubernetes modifying objects I’ve created. Okay, it’s perfectly fine for the scheduler to assign a node to my pods after I create them, but Kubernetes modifying the labels of my pods and the label selectors of my ReplicationControllers is something that I don’t expect and could cause me to go around the office yelling at my colleagues, “Who’s been messing with my controllers!?!?”  But even more importantly, if you’ve paid close attention to the words I’ve used, you probably noticed that all this time I said explicitly that the kubectl client was the one performing all these steps of the rolling update.

> But why is it such a bad thing that the update process is being performed by the client instead of on the server? Well, in your case, the update went smoothly, but what if you lost network connectivity while kubectl was performing the update? The update process would be interrupted mid-way. Pods and ReplicationControllers would end up in an intermediate state.

> Another reason why performing an update like this isn’t as good as it could be is because it’s imperative. Throughout this book, I’ve stressed how Kubernetes is about you telling it the desired state of the system and having Kubernetes achieve that state on its own, by figuring out the best way to do it. This is how pods are deployed and how pods are scaled up and down. You never tell Kubernetes to add an additional pod or remove an excess one—you change the number of desired replicas and that’s it.

> When using a Deployment, the actual pods are created and managed by the Deployment’s ReplicaSets, not by the Deployment directly.

> A Deployment is a higher-level resource meant for deploying applications and updating them declaratively, instead of doing it through a ReplicationController or a ReplicaSet, which are both considered lower-level concepts.

> Using a Deployment instead of the lower-level constructs makes updating an app much easier, because you’re defining the desired state through the single Deployment resource and letting Kubernetes take care of the rest, as you’ll see in the next few pages.

> The three pods created by the Deployment include an additional numeric value in the middle of their names. What is that exactly? The number corresponds to the hashed value of the pod template in the Deployment and the ReplicaSet managing these pods. As we said earlier, a Deployment doesn’t manage pods directly. Instead, it creates ReplicaSets and leaves the managing to them.

> Previously, when you ran your app using a ReplicationController, you had to explicitly tell Kubernetes to perform the update by running kubectl rolling-update. You even had to specify the name for the new ReplicationController that should replace the old one. Kubernetes replaced all the original pods with new ones and deleted the original ReplicationController at the end of the process. During the process, you basically had to stay around, keeping your terminal open and waiting for kubectl to finish the rolling update.
Now compare this to how you’re about to update a Deployment. The only thing you need to do is modify the pod template defined in the Deployment resource and Kubernetes will take all the steps necessary to get the actual system state to what’s defined in the resource. Similar to scaling a ReplicationController or ReplicaSet up or down, all you need to do is reference a new image tag in the Deployment’s pod template and leave it to Kubernetes to transform your system so it matches the new desired state.

> The events that occurred below the Deployment’s surface during the update are similar to what happened during the kubectl rolling-update. An additional ReplicaSet was created and it was then scaled up slowly, while the previous ReplicaSet was scaled down to zero.

> Luckily, Deployments make it easy to roll back to the previously deployed version by telling Kubernetes to undo the last rollout of a Deployment: $ kubectl rollout undo deployment kubia

> The revision history can be displayed with the kubectl rollout history command: $ kubectl rollout history deployment kubia deployments "kubia"

> Remember the inactive ReplicaSet left over when you modified the Deployment the first time? The ReplicaSet represents the first revision of your Deployment. All Replica-Sets created by a Deployment represent the complete revision history. Each ReplicaSet stores the complete information of the Deployment at that specific revision, so you shouldn’t delete it manually. If you do, you’ll lose that specific revision from the Deployment’s history, preventing you from rolling back to it.

> Two properties affect how many pods are replaced at once during a Deployment’s rolling update. They are maxSurge and maxUnavailable and can be set as part of the rollingUpdate sub-property of the Deployment’s strategy attribute.

> If you only define the readiness probe without setting minReadySeconds properly, new pods are considered available immediately when the first invocation of the readiness probe succeeds. If the readiness probe starts failing shortly after, the bad version is rolled out across all pods. Therefore, you should set minReadySeconds appropriately.

## Chapter 10 - StatefulSets: deploying replicated stateful applications

> Because the reference to the claim is in the pod template, which is used to stamp out multiple pod replicas, you can’t make each replica use its own separate Persistent-VolumeClaim. You can’t use a ReplicaSet to run a distributed data store, where each instance needs its own separate storage—at least not by using a single ReplicaSet.

> In addition to storage, certain clustered applications also require that each instance has a long-lived stable identity. Pods can be killed from time to time and replaced with new ones. When a ReplicaSet replaces a pod, the new pod is a completely new pod with a new hostname and IP, although the data in its storage volume may be that of the killed pod. For certain apps, starting up with the old instance’s data but with a completely new network identity may cause problems.

> Why do certain apps mandate a stable network identity? This requirement is fairly common in distributed stateful applications. Certain apps require the administrator to list all the other cluster members and their IP addresses (or hostnames) in each member’s configuration file. But in Kubernetes, every time a pod is rescheduled, the new pod gets both a new hostname and a new IP address, so the whole application cluster would have to be reconfigured every time one of its members is rescheduled.

> Instead of using a ReplicaSet to run these types of pods, you create a StatefulSet resource, which is specifically tailored to applications where instances of the application must be treated as non-fungible individuals, with each one having a stable name and state.

> Pod replicas managed by a ReplicaSet or ReplicationController are much like cattle.
Because they’re mostly stateless, they can be replaced with a completely new pod replica at any time. Stateful pods require a different approach. When a stateful pod instance dies (or the node it’s running on fails), the pod instance needs to be resurrected on another node, but the new instance needs to get the same name, network identity, and state as the one it’s replacing. This is what happens when the pods are managed through a StatefulSet.

> Unlike pods created by ReplicaSets, pods created by the StatefulSet aren’t exact replicas of each other. Each can have its own set of volumes—in other words, storage (and thus persistent state)—which differentiates it from its peers. Pet pods also have a predictable (and stable) identity instead of each new pod instance getting a completely random one.

> For this reason, a StatefulSet requires you to create a corresponding governing headless Service that’s used to provide the actual network identity to each pod. Through this Service, each pod gets its own DNS entry, so its peers and possibly other clients in the cluster can address the pod by its hostname. For example, if the governing Service belongs to the default namespace and is called foo, and one of the pods is called A-0, you can reach the pod through its fully qualified domain name, which is a-0.foo.default.svc.cluster.local. You can’t do that with pods managed by a ReplicaSet.

> Scale up from two to three instances, the new instance will get index 2 (the exist-ing instances obviously have indexes 0 and 1). The nice thing about scaling down a StatefulSet is the fact that you always know what pod will be removed.

> Scaling down a StatefulSet always removes the instances with the highest ordinal index first.

> Each stateful pod instance needs to use its own storage, plus if a state-ful pod is rescheduled (replaced with a new instance but with the same identity as before), the new instance must have the same storage attached to it.

> The StatefulSet has to create the PersistentVolumeClaims as well, the same way it’s creating the pods. For this reason, a StatefulSet can also have one or more volume claim templates, which enable it to stamp out PersistentVolumeClaims along with each pod instance.

> Scaling up a StatefulSet by one creates two or more API objects (the pod and one or more PersistentVolumeClaims referenced by the pod). Scaling down, however, deletes only the pod, leaving the claims alone. The reason for this is obvious, if you consider what happens when a claim is deleted. After a claim is deleted, the PersistentVolume it was bound to gets recycled or deleted and its contents are lost.

> Because stateful pods are meant to run stateful applications, which implies that the data they store in the volume is important, deleting the claim on scale-down of a Stateful-Set could be catastrophic—especially since triggering a scale-down is as simple as decreasing the replicas field of the StatefulSet. For this reason, you’re required to delete PersistentVolumeClaims manually to release the underlying PersistentVolume.

> As explained earlier, before deploying a StatefulSet, you first need to create a headless Service, which will be used to provide the network identity for your stateful pods. The following listing shows the Service manifest.

> The StatefulSet manifest isn’t that different from ReplicaSet or Deployment manifests you’ve created so far. What’s new is the volumeClaimTemplates list. In it, you’re defining one volume claim template called data, which will be used to create a Persistent-VolumeClaim for each pod.

> Notice anything strange? Remember how a ReplicationController or a ReplicaSet creates all the pod instances at the same time? Your StatefulSet is configured to create two replicas, but it created a single pod.

> Don’t worry, nothing is wrong. The second pod will be created only after the first one is up and ready. StatefulSets behave this way because certain clustered stateful apps are sensitive to race conditions if two or more cluster members come up at the same time, so it’s safer to bring each member up fully before continuing to bring up the rest.

> The key thing to remember is that scaling down (and up) is performed gradually—similar to how individual pods are created when the StatefulSet is created initially. When scaling down by more than one instance, the pod with the highest ordinal number is deleted first. Only after the pod terminates completely is the pod with the second highest ordinal number deleted.

> Instead of using a piggyback pod to access the service from inside the cluster, you can use the same proxy feature provided by the API server to access the service the way you’ve accessed individual pods.

> We still need to cover one more important thing. An important requirement of clustered apps is peer discovery—the ability to find other members of the cluster. Each member of a StatefulSet needs to easily find all the other members. Sure, it could do that by talking to the API server, but one of Kubernetes’ aims is to expose features that help keep applications completely Kubernetes-agnostic. Having apps talk to the Kubernetes API is therefore undesirable.

> How can a pod discover its peers without talking to the API? Is there an existing, well-known technology you can use that makes this possible? How about the Domain Name System (DNS)? Depending on how much you know about DNS, you probably understand what an A, CNAME, or MX record is used for. Other lesser-known types of DNS records also exist. One of them is the SRV record.

## Chapter 11 - Understanding Kubernetes internals

> $ kubectl get componentstatuses

> Kubernetes system components communicate only with the API server. They don’t talk to each other directly. The API server is the only component that communicates with etcd. None of the other components communicate with etcd directly, but instead modify the cluster state by talking to the API server.

> Connections between the API server and the other components are almost always initiated by the components, as shown in figure 11.1. But the API server does connect to the Kubelet when you use kubectl to fetch logs, use kubectl attach to connect to a running container, or use the kubectl port-forward command.

> The Kubelet is the only component that always runs as a regular system compo-nent, and it’s the Kubelet that then runs all the other components as pods. To run the Control Plane components as pods, the Kubelet is also deployed on the master. The next listing shows pods in the kube-system namespace in a cluster created with kubeadm, which is explained in appendix B.

> All the objects you’ve created throughout this book—Pods, ReplicationControllers, Services, Secrets, and so on—need to be stored somewhere in a persistent manner so their manifests survive API server restarts and failures. For this, Kubernetes uses etcd

> It’s worth emphasizing that etcd is the only place Kubernetes stores cluster state and metadata.

> etcdctl ls /registry

> Prior to Kubernetes version 1.7, the JSON manifest of a Secret resource was also stored like this (it wasn’t encrypted). If someone got direct access to etcd, they knew all your Secrets. From version 1.7, Secrets are encrypted and thus stored much more securely.

> Kubernetes improves this by requiring all other Control Plane components to go through the API server. This way updates to the cluster state are always consistent, because the optimistic locking mechanism is implemented in a single place, so less chance exists, if any, of error. The API server also makes sure that the data written to the store is always valid and that changes to the data are only performed by authorized clients.

> etcd is usually deployed with an odd number of instances. I’m sure you’d like to know why. Let’s compare having two vs. having one instance. Having two instances requires both instances to be present to have a majority. If either of them fails, the etcd cluster can’t transition to a new state because no majority exists. Having two instances is worse than having only a single instance. By having two, the chance of the whole cluster fail-ing has increased by 100%, compared to that of a single-node cluster failing.

> The Kubernetes API server is the central component used by all other components and by clients, such as kubectl. It provides a CRUD (Create, Read, Update, Delete) interface for querying and modifying the cluster state over a RESTful API. It stores that state in etcd.

> In addition to providing a consistent way of storing objects in etcd, it also performs validation of those objects, so clients can’t store improperly configured objects (which they could if they were writing to the store directly). Along with validation, it also han-dles optimistic locking, so changes to an object are never overridden by other clients in the event of concurrent updates.

> Examples of Admission Control plugins include  AlwaysPullImages—Overrides the pod’s imagePullPolicy to Always, forcing the image to be pulled every time the pod is deployed.
 ServiceAccount—Applies the default service account to pods that don’t specify it explicitly.
 NamespaceLifecycle—Prevents creation of pods in namespaces that are in the process of being deleted, as well as in non-existing namespaces.
 ResourceQuota—Ensures pods in a certain namespace only use as much CPU and memory as has been allotted to the namespace. We’ll learn more about this in chapter 14.

> The API server doesn’t do anything else except what we’ve discussed. For example, it doesn’t create pods when you create a ReplicaSet resource and it doesn’t manage the endpoints of a service. That’s what controllers in the Controller Manager do.

> You’ve already learned that you don’t usually specify which cluster node a pod should run on. This is left to the Scheduler. From afar, the operation of the Scheduler looks simple. All it does is wait for newly created pods through the API server’s watch mech-anism and assign a node to each new pod that doesn’t already have the node set.

> Pods belonging to the same Service or ReplicaSet are spread across multiple nodes by default. It’s not guaranteed that this is always the case. But you can force pods to be spread around the cluster or kept close together by defining pod affinity and anti-affinity rules, which are explained in chapter 16.

> Instead of running a single Scheduler in the cluster, you can run multiple Schedulers.
Then, for each pod, you specify the Scheduler that should schedule this particular pod by setting the schedulerName property in the pod spec. Pods without this property set are scheduled using the default Scheduler, and so are pods with schedulerName set to default-scheduler. All other pods are ignored by the default Scheduler, so they need to be scheduled either manually or by another Scheduler watching for such pods.

> As previously mentioned, the API server doesn’t do anything except store resources in etcd and notify clients about the change. The Scheduler only assigns a node to the pod, so you need other active components to make sure the actual state of the system converges toward the desired state, as specified in the resources deployed through the API server. This work is done by controllers running inside the Controller Manager.

> The Scheduler doesn’t instruct the selected node (or the Kubelet running on that node) to run the pod. All the Scheduler does is update the pod definition through the API server. The API server then notifies the Kubelet (again, through the watch mech-anism described previously) that the pod has been scheduled. As soon as the Kubelet on the target node sees the pod has been scheduled to its node, it creates and runs the pod’s containers.

> Controllers do many different things, but they all watch the API server for changes to resources (Deployments, Services, and so on) and perform operations for each change, whether it’s a creation of a new object or an update or deletion of an existing object.
Most of the time, these operations include creating other resources or updating the watched resources themselves (to update the object’s status, for example).

> Although the Kubelet talks to the Kubernetes API server and gets the pod manifests from there, it can also run pods based on pod manifest files in a specific local direc-tory as shown in figure 11.8. This feature is used to run the containerized versions of the Control Plane components as pods, as you saw in the beginning of the chapter.

> Beside the Kubelet, every worker node also runs the kube-proxy, whose purpose is to make sure clients can connect to the services you define through the Kubernetes API.
The kube-proxy makes sure connections to the service IP and port end up at one of the pods backing that service (or other, non-pod service endpoints). When a service is backed by more than one pod, the proxy performs load balancing across those pods.

> The DNS server pod is exposed through the kube-dns service, allowing the pod to be moved around the cluster, like any other pod. The service’s IP address is specified as the nameserver in the /etc/resolv.conf file inside every container deployed in the cluster.

> By now, you know that each pod gets its own unique IP address and can communicate with all other pods through a flat, NAT-less network. How exactly does Kubernetes achieve this? In short, it doesn’t. The network is set up by the system administrator or by a Container Network Interface (CNI) plugin, not by Kubernetes itself.

> When pod A connects to (sends a network packet to) pod B, the source IP pod B sees must be the same IP that pod A sees as its own. There should be no network address translation (NAT) performed in between—the packet sent by pod A must reach pod B with both the source and destination address unchanged.

> The requirement for NAT-less communication between pods also extends to pod-to-node and node-to-pod communication. But when a pod communicates with ser-vices out on the internet, the source IP of the packets the pod sends does need to be changed, because the pod’s IP is private. The source IP of outbound packets is changed to the host worker node’s IP address.

> A key detail of Services is that they consist of an IP and port pair (or multiple IP and port pairs in the case of multi-port Services), so the service IP by itself doesn’t represent anything. That’s why you can’t ping them.

> When a service is created in the API server, the virtual IP address is assigned to it immediately. Soon afterward, the API server notifies all kube-proxy agents running on the worker nodes that a new Service has been created. Then, each kube-proxy makes that service addressable on the node it’s running on. It does this by setting up a few iptables rules, which make sure each packet destined for the service IP/port pair is intercepted and its destination address modified, so the packet is redirected to one of the pods backing the service.

> An Endpoints object holds the IP/port pairs of all the pods that back the service (an IP/port pair can also point to something other than a pod). That’s why the kube-proxy must also watch all Endpoints objects. After all, an Endpoints object changes every time a new backing pod is created or deleted, and when the pod’s readiness status changes or the pod’s labels change and it falls in or out of scope of the service.

## Chapter 12 -

> Users are meant to be managed by an external system, such as a Single Sign On (SSO) system, but the pods use a mechanism called service accounts, which are cre-ated and stored in the cluster as ServiceAccount resources. In contrast, no resource represents user accounts, which means you can’t create, update, or delete users through the API server.

> A ServiceAccount’s image pull Secrets behave slightly differently than its mountable Secrets. Unlike mountable Secrets, they don’t determine which image pull Secrets a pod can use, but which ones are added automatically to all pods using the Service-Account. Adding image pull Secrets to a ServiceAccount saves you from having to add them to each pod individually.

> When your cluster isn’t using proper authorization, creating and using additional ServiceAccounts doesn’t make much sense, since even the default ServiceAccount is allowed to do anything. The only reason to use ServiceAccounts in that case is to enforce mountable Secrets or to provide image pull Secrets through the Service-Account, as explained earlier. 
 But creating additional ServiceAccounts is practically a must when you use the RBAC authorization plugin, which we’ll explore next.

> The default Service-Account isn’t allowed to view cluster state, let alone modify it in any way, unless you grant it additional privileges. To write apps that communicate with the Kubernetes API server (as described in chapter 8), you need to understand how to manage authorization through RBAC-specific resources.

> To write apps that communicate with the Kubernetes API server (as described in chapter 8), you need to understand how to manage authorization through RBAC-specific resources.

> The default permissions for a ServiceAccount don’t allow it to list or modify any resources

> A regular Role only allows access to resources in the same namespace the Role is in. If you want to allow someone access to resources across different namespaces, you have to create a Role and RoleBinding in every one of those namespaces. If you want to extend this to all namespaces (this is something a cluster administrator would prob-ably need), you need to create the same Role and RoleBinding in each namespace.
When creating an additional namespace, you have to remember to create the two resources there as well.

> We’ve mentioned that the API server also exposes non-resource URLs. Access to these URLs must also be granted explicitly; otherwise the API server will reject the client’s request. Usually, this is done for you automatically through the system:discovery ClusterRole and the identically named ClusterRoleBinding, which appear among other predefined ClusterRoles and ClusterRoleBindings (we’ll explore them in sec-tion 12.2.5).

> ClusterRoles don’t always need to be bound with cluster-level ClusterRoleBindings.
They can also be bound with regular, namespaced RoleBindings. You’ve already started looking at predefined ClusterRoles, so let’s look at another one called view, which is shown in the following listing.

> If you create a ClusterRoleBinding and reference the Cluster-Role in it, the subjects listed in the binding can view the specified resources across all namespaces. If, on the other hand, you create a RoleBinding, the subjects listed in the binding can only view resources in the namespace of the RoleBinding.

> You now have a RoleBinding in the foo namespace, binding the default Service-Account in that same namespace with the view ClusterRole. What can your pod access now? As you can see, your pod can list pods in the foo namespace, but not in any other spe-cific namespace or across all namespaces.

> It’s a good idea to create a specific ServiceAccount for each pod (or a set of pod rep-licas) and then associate it with a tailor-made Role (or a ClusterRole) through a RoleBinding (not a ClusterRoleBinding, because that would give the pod access to resources in other namespaces, which is probably not what you want).

## Chapter 13 -

> Containers in a pod usually run under separate Linux namespaces, which isolate their processes from processes running in other containers or in the node’s default namespaces. 
 For example, we learned that each pod gets its own IP and port space, because it uses its own network namespace. Likewise, each pod has its own process tree, because it has its own PID namespace, and it also uses its own IPC namespace, allowing only processes in the same pod to communicate with each other through the Inter-Process Communication mechanism (IPC).

> Certain pods (usually system pods) need to operate in the host’s default namespaces, allowing them to see and manipulate node-level resources and devices. For example, a pod may need to use the node’s network adapters instead of its own virtual network adapters. This can be achieved by setting the hostNetwork property in the pod spec to true.

> A related feature allows pods to bind to a port in the node’s default namespace, but still have their own network namespace. This is done by using the hostPort property in one of the container’s ports defined in the spec.containers.ports field.
 Don’t confuse pods using hostPort with pods exposed through a NodePort service.
They’re two different things, as explained in figure 13.2.
 The first thing you’ll notice in the figure is that when a pod is using a hostPort, a connection to the node’s port is forwarded directly to the pod running on that node, whereas with a NodePort service, a connection to the node’s port is forwarded to a randomly selected pod (possibly on another node). The other difference is that with pods using a hostPort, the node’s port is only bound on nodes that run such pods, whereas NodePort services bind the port on all nodes, even on those that don’t run such a pod (as on node 3 in the figure).

> The hostPort feature is primarily used for exposing system services, which are deployed to every node using DaemonSets. Initially, people also used it to ensure two replicas of the same pod were never scheduled to the same node, but now you have a better way of achieving this—it’s explained in chapter 16.

> Similar to the hostNetwork option are the hostPID and hostIPC pod spec properties.
When you set them to true, the pod’s containers will use the node’s PID and IPC namespaces, allowing processes running in the containers to see all the other pro-cesses on the node or communicate with them through IPC, respectively. See the fol-lowing listing for an example.

> What user the container runs as is specified in the container image. In a Dockerfile, this is done using the USER directive. If omitted, the container runs as root.

> Although containers are mostly isolated from the host system, running their pro-cesses as root is still considered a bad practice. For example, when a host directory is mounted into the container, if the process running in the container is running as root, it has full access to the mounted directory, whereas if it’s running as non-root, it won’t.

> An example of such a pod is the kube-proxy pod, which needs to modify the node’s iptables rules to make services work, as was explained in chapter 11. If you follow the instructions in appendix B and deploy a cluster with kubeadm, you’ll see every cluster node runs a kube-proxy pod and you can examine its YAML specification to see all the special features it’s using.

> In the previous section, you saw one way of giving a container unlimited power. In the old days, traditional UNIX implementations only distinguished between privileged and unprivileged processes, but for many years, Linux has supported a much more fine-grained permission system through kernel capabilities.

> To prevent the container from doing that, you need to drop the capability by listing it under the container’s securityContext.capabilities.drop property

> These types of attacks can be thwarted by preventing the container from writing to its filesystem, where the app’s executable code is normally stored. This is done by set-ting the container’s securityContext.readOnlyRootFilesystem property to true, as shown in the following listing.

> As shown in the example, when you make the container’s filesystem read-only, you’ll probably want to mount a volume in every directory the application writes to (for example, logs, on-disk caches, and so on).
TIP To increase security, when running pods in production, set their con-tainer’s readOnlyRootFilesystem property to true.

> If those two containers use a volume to share files, they may not necessarily be able to read or write files of one another. 
 That’s why Kubernetes allows you to specify supplemental groups for all the pods running in the container, allowing them to share files, regardless of the user IDs they’re running as. This is done using the following two properties:
 fsGroup  supplementalGroups

> The examples in the previous sections have shown how a person deploying pods can do whatever they want on any cluster node, by deploying a privileged pod to the node, for example. Obviously, a mechanism must prevent users from doing part or all of what’s been explained. The cluster admin can restrict the use of the previously described security-related features by creating one or more PodSecurityPolicy resources.

> PodSecurityPolicy is a cluster-level (non-namespaced) resource, which defines what security-related features users can or can’t use in their pods.

> Changing the policy has no effect on existing pods, because PodSecurity-Policies are enforced only when creating or updating pods.

> For the runAsUser field an additional rule can be used: MustRunAsNonRoot. As the name suggests, it prevents users from deploying containers that run as root. Either the container spec must specify a runAsUser field, which can’t be zero (zero is the root user’s ID), or the container image itself must run as a non-zero user ID. We explained why this is a good thing earlier.

> The last thing a PodSecurityPolicy resource can do is define which volume types users can add to their pods. At the minimum, a PodSecurityPolicy should allow using at least the emptyDir, configMap, secret, downwardAPI, and the persistentVolume-Claim volumes.

> Assigning different policies to different users is done through the RBAC mecha-nism described in the previous chapter. The idea is to create as many policies as you need and make them available to individual users or groups by creating ClusterRole resources and pointing them to the individual policies by name. By binding those ClusterRoles to specific users or groups with ClusterRoleBindings, when the Pod-SecurityPolicy Admission Control plugin needs to decide whether to admit a pod defi-nition or not, it will only consider the policies accessible to the user creating the pod.

> But how do you authenticate as Alice or Bob instead of whatever you’re authenticated as currently? The book’s appendix A explains how kubectl can be used with multiple clusters, but also with multiple contexts. A context includes the user credentials used for talking to a cluster. Turn to appendix A to find out more. Here we’ll show the bare commands enabling you to use kubectl as Alice or Bob. kubectl config set-credentials alice --username=alice --password=password

> You can now try creating a privileged pod while authenticating as Alice. You can tell kubectl which user credentials to use by using the --user option:

> Whether this is configurable or not depends on which container networking plugin is used in the cluster. If the networking plugin supports it, you can configure network isolation by creating NetworkPolicy resources.

> A NetworkPolicy applies to pods that match its label selector and specifies either which sources can access the matched pods or which destinations can be accessed from the matched pods. This is configured through ingress and egress rules, respec-tively. Both types of rules can match only the pods that match a pod selector, all pods in a namespace whose labels match a namespace selector, or a network IP block specified using Classless Inter-Domain Routing (CIDR) notation (for example, 192.168.1.0/24).

> By default, pods in a given namespace can be accessed by anyone. First, you’ll need to change that. You’ll create a default-deny NetworkPolicy, which will prevent all clients from connecting to any pod in your namespace.

> In a multi-tenant Kubernetes cluster, tenants usually can’t add labels (or annotations) to their namespaces themselves. If they could, they’d be able to circumvent the namespaceSelector-based ingress rules.

## Chapter 14 -

> When you don’t specify a request for CPU, you’re saying you don’t care how much CPU time the process running in your container is allotted. In the worst case, it may not get any CPU time at all (this happens when a heavy demand by other processes exists on the CPU). Although this may be fine for low-priority batch jobs, which aren’t time-critical, it obviously isn’t appropriate for containers handling user requests.

> By specifying resource requests, you’re specifying the minimum amount of resources your pod needs. This information is what the Scheduler uses when scheduling the pod to a node. Each node has a certain amount of CPU and memory it can allocate to pods. When scheduling a pod, the Scheduler will only consider nodes with enough unallocated resources to meet the pod’s resource requirements. If the amount of unallocated CPU or memory is less than what the pod requests, Kubernetes will not schedule the pod to that node, because the node can’t provide the minimum amount required by the pod.

> What’s important and somewhat surprising here is that the Scheduler doesn’t look at how much of each individual resource is being used at the exact time of scheduling but at the sum of resources requested by the existing pods deployed on the node.

> Among others, two prioritization functions rank nodes based on the amount of resources requested: LeastRequestedPriority and MostRequestedPriority. The first one prefers nodes with fewer requested resources (with a greater amount of unallocated resources), whereas the second one is the exact opposite—it prefers nodes that have the most requested resources (a smaller amount of unallocated CPU and memory).

> The CPU requests don’t only affect scheduling—they also determine how the remaining (unused) CPU time is distributed between pods. Because your first pod requested 200 millicores of CPU and the other one 1,000 millicores, any unused CPU will be split among the two pods in a 1 to 5 ratio

> But if one container wants to use up as much CPU as it can, while the other one is sit-ting idle at a given moment, the first container will be allowed to use the whole CPU time (minus the small amount of time used by the second container, if any). After all, it makes sense to use all the available CPU if no one else is using it, right? As soon as the second container needs CPU time, it will get it and the first container will be throt-tled back.

> CPU is a compressible resource, which means the amount used by a container can be throttled without affecting the process running in the container in an adverse way.
Memory is obviously different—it’s incompressible. Once a process is given a chunk of memory, that memory can’t be taken away from it until it’s released by the process itself. That’s why you need to limit the maximum amount of memory a container can be given.

> Because you haven’t specified any resource requests, they’ll be set to the same values as the resource limits.

> Unlike resource requests, resource limits aren’t constrained by the node’s allocatable resource amounts. The sum of all limits of all the pods on a node is allowed to exceed 100% of the node’s capacity (figure 14.3). Restated, resource limits can be overcom-mitted. This has an important consequence—when 100% of the node’s resources are used up, certain containers will need to be killed.

> You’ve already learned that CPU is a compressible resource, and it’s only natural for a process to want to consume all of the CPU time when not waiting for an I/O operation. As you’ve learned, a process’ CPU usage is throttled, so when a CPU limit is set for a container, the process isn’t given more CPU time than the config-ured limit.

> With memory, it’s different. When a process tries to allocate memory over its limit, the process is killed (it’s said the container is OOMKilled, where OOM stands for Out Of Memory). If the pod’s restart policy is set to Always or OnFailure, the process is restarted immediately, so you may not even notice it getting killed. But if it keeps going over the memory limit and getting killed, Kubernetes will begin restart-ing it with increasing delays between restarts. You’ll see a CrashLoopBackOff status in that case

> The CrashLoopBackOff status doesn’t mean the Kubelet has given up. It means that after each crash, the Kubelet is increasing the time period before restarting the con-tainer. After the first crash, it restarts the container immediately and then, if it crashes again, waits for 10 seconds before restarting it again. On subsequent crashes, this delay is then increased exponentially to 20, 40, 80, and 160 seconds, and finally lim-ited to 300 seconds. Once the interval hits the 300-second limit, the Kubelet keeps restarting the container indefinitely every five minutes until the pod either stops crashing or is deleted.

> You may want to use the Downward API to pass the CPU limit to the container and use it instead of relying on the number of CPUs your app can see on the system. You can also tap into the cgroups system directly to get the configured CPU limit

> Imagine having two pods, where pod A is using, let’s say, 90% of the node’s mem-ory and then pod B suddenly requires more memory than what it had been using up to that point and the node can’t provide the required amount of memory. Which container should be killed? Should it be pod B, because its request for memory can’t be satisfied, or should pod A be killed to free up memory, so it can be provided to pod B? 
 Obviously, it depends. Kubernetes can’t make a proper decision on its own. You need a way to specify which pods have priority in such cases. Kubernetes does this by categorizing pods into three Quality of Service (QoS) classes:
 BestEffort (the lowest priority)  Burstable  Guaranteed (the highest)

> You might expect these classes to be assignable to pods through a separate field in the manifest, but they aren’t. The QoS class is derived from the combination of resource requests and limits for the pod’s containers. Here’s how.

> The lowest priority QoS class is the BestEffort class. It’s assigned to pods that don’t have any requests or limits set at all (in any of their containers). This is the QoS class that has been assigned to all the pods you created in previous chapters. Containers running in these pods have had no resource guarantees whatsoever. In the worst case, they may get almost no CPU time at all and will be the first ones killed when memory needs to be freed for other pods. But because a BestEffort pod has no memory limits set, its containers may use as much memory as they want, if enough memory is available.

> pods whose containers’ requests are equal to the limits for all resources. For a pod’s class to be Guaranteed, three things need to be true:
 Requests and limits need to be set for both CPU and memory.
 They need to be set for each container.
 They need to be equal (the limit needs to match the request for each resource in each container).
Because a container’s resource requests, if not set explicitly, default to the limits, specifying the limits for all resources (for each container in the pod) is enough

> In between BestEffort and Guaranteed is the Burstable QoS class. All other pods fall into this class. This includes single-container pods where the container’s limits don’t match its requests and all pods where at least one container has a resource request specified, but not the limit. It also includes pods where one container’s requests match their limits, but another container has no requests or limits specified.
Burstable pods get the amount of resources they request, but are allowed to use addi-tional resources (up to the limit) if needed.

> For multi-container pods, if all the containers have the same QoS class, that’s also the pod’s QoS class. If at least one container has a different class, the pod’s QoS class is Burstable, regardless of what the container classes are.

> When the system is overcommitted, the QoS classes determine which container gets killed first so the freed resources can be given to higher priority pods. First in line to get killed are pods in the BestEffort class, followed by Burstable pods, and finally Guaranteed pods, which only get killed if system processes need memory.

> Each running process has an OutOfMemory (OOM) score. The system selects the process to kill by comparing OOM scores of all the running processes. When memory needs to be freed, the process with the highest score gets killed.
421Setting default requests and limits for pods per namespace memory than the other, percentage-wise. That’s why in figure 14.5, pod B, using 90% of its requested memory, gets killed before pod C, which is only using 70%, even though it’s using more megabytes of memory than pod B.

> Instead of having to do this for every container, you can also do it by creating a Limit-Range resource. It allows you to specify (for each namespace) not only the minimum and maximum limit you can set on a container for each resource, but also the default resource requests for containers that don’t specify requests explicitly, as depicted in figure 14.6.

> The limits specified in a LimitRange resource apply to each individual pod/con-tainer or other kind of object created in the same namespace as the LimitRange object. They don’t limit the total amount of resources available across all the pods in the namespace. This is specified through ResourceQuota objects

> As you can see from the previous example, the minimum and maximum limits for a whole pod can be configured. They apply to the sum of all the pod’s containers’ requests and limits.

> The example shows a single LimitRange object containing limits for everything, but you could also split them into multiple objects if you prefer to have them orga-nized per type (one for pod limits, another for container limits, and yet another for PVCs, for example). Limits from multiple LimitRange objects are all consolidated when validating a pod or PVC.

> A ResourceQuota limits the amount of computational resources the pods and the amount of storage PersistentVolumeClaims in a namespace can consume. It can also limit the number of pods, claims, and other API objects users are allowed to create inside the namespace. Because you’ve mostly dealt with CPU and memory so far, let’s start by looking at how to specify quotas for them.

> A ResourceQuota object applies to the namespace it’s created in, like a Limit-Range, but it applies to all the pods’ resource requests and limits in total and not to each individual pod or container separately

> After you post the ResourceQuota object to the API server, you can use the kubectl describe command to see how much of the quota is already used up, as shown in the following listing.

> When a quota for a specific resource (CPU or memory) is configured (request or limit), pods need to have the request or limit (respectively) set for that same resource;
otherwise the API server will not accept the pod. That’s why having a LimitRange with defaults for those resources can make life a bit easier for people creating pods.

> A ResourceQuota object can also limit the amount of persistent storage that can be claimed in the namespace, as shown in the following listing.

> A ResourceQuota can also be configured to limit the number of Pods, Replication-Controllers, Services, and other objects inside a single namespace. This allows the cluster admin to limit the number of objects users can create based on their payment plan, for example, and can also limit the number of public IPs or node ports Ser-vices can use.

## Chapter 15 -

> Kubernetes can monitor your pods and scale them up automatically as soon as it detects an increase in the CPU usage or some other metric. If running on a cloud infrastructure, it can even spin up additional nodes if the existing ones can’t accept any more pods.

> The Autoscaler doesn’t perform the gathering of the pod metrics itself. It gets the metrics from a different source. As we saw in the previous chapter, pod and node met-rics are collected by an agent called cAdvisor, which runs in the Kubelet on each node

> autoscaling is based on multiple pod metrics (for example, both CPU usage and Queries-Per-Second [QPS]), the calculation isn’t that much more complicated.
The Autoscaler calculates the replica count for each metric individually and then takes the highest value

> Perhaps the most important metric you’ll want to base autoscaling on is the amount of CPU consumed by the processes running inside your pods. Imagine having a few pods providing a service. When their CPU usage reaches 100% it’s obvious they can’t cope with the demand anymore and need to be scaled either up (vertical scaling—increas-ing the amount of CPU the pods can use) or out (horizontal scaling—increasing the number of pods).

> Always set the target CPU usage well below 100% (and definitely never above 90%) to leave enough room for handling sudden load spikes.

> As far as the Autoscaler is concerned, only the pod’s guaranteed CPU amount (the CPU requests) is important when determining the CPU utilization of a pod. The Auto-scaler compares the pod’s actual CPU consumption and its CPU requests, which means the pods you’re autoscaling need to have CPU requests set (either directly or indirectly through a LimitRange object) for the Autoscaler to determine the CPU uti-lization percentage.
CREATING A HORIZONTAL

> After creating the Deployment, to enable horizontal autoscaling of its pods, you need to create a HorizontalPodAutoscaler (HPA) object and point it to the Deploy-ment. You could prepare and post the YAML manifest for the HPA, but an easier way exists—using the kubectl autoscale command:

> kubectl run -it --rm --restart=Never loadgenerator --image=busybox ➥ -- sh -c "while true; do wget -O - -q http://kubia.default; done"

> Additionally, it has a limit on how soon a subsequent autoscale operation can occur after the previous one. Currently, a scale-up will occur only if no rescaling event occurred in the last three minutes. A scale-down event is performed even less frequently—every five minutes. Keep this in mind so you don’t wonder why the autoscaler refuses to perform a rescale operation even if the metrics clearly show that it should.

> Memory-based autoscaling is much more problematic than CPU-based autoscal-ing. The main reason is because after scaling up, the old pods would somehow need to be forced to release memory. This needs to be done by the app itself—it can’t be done by the system. All the system could do is kill and restart the app, hoping it would use less memory than before. But if the app then uses the same amount as before, the Autoscaler would scale it up again. And again, and again, until it reaches the maxi-mum number of pods configured on the HPA resource. Obviously, this isn’t what any-one wants. Memory-based autoscaling was introduced in Kubernetes version 1.8, and is configured exactly like CPU-based autoscaling.

> As you can see, the metrics field allows you to define more than one metric to use.
In the listing, you’re using a single metric. Each entry defines the type of metric— in this case, a Resource metric. You have three types of metrics you can use in an HPA object:
 Resource  Pods  Object

> You need to understand that not all metrics are appropriate for use as the basis of autoscaling. As mentioned previously, the pods’ containers’ memory consumption isn’t a good metric for autoscaling. The autoscaler won’t function properly if increasing the number of replicas doesn’t result in a linear decrease of the average value of the observed metric (or at least close to linear).

> The Cluster Autoscaler takes care of automatically provisioning additional nodes when it notices a pod that can’t be scheduled to existing nodes because of a lack of resources on those nodes. It also de-provisions nodes when they’re underutilized for longer periods of time.

> The Cluster Autoscaler also needs to scale down the number of nodes when they aren’t being utilized enough. The Autoscaler does this by monitoring the requested CPU and memory on all the nodes. If the CPU and memory requests of all the pods running on a given node are below 50%, the node is considered unnecessary.

> The Autoscaler also checks to see if any system pods are running (only) on that node (apart from those that are run on every node, because they’re deployed by a Daemon-Set, for example). If a system pod is running on a node, the node won’t be relinquished.
The same is also true if an unmanaged pod or a pod with local storage is running on the node, because that would cause disruption to the service the pod is providing. In other words, a node will only be returned to the cloud provider if the Cluster Autoscaler knows the pods running on the node will be rescheduled to other nodes.

> Certain services require that a minimum number of pods always keeps running;
this is especially true for quorum-based clustered applications. For this reason, Kuber-netes provides a way of specifying the minimum number of pods that need to keep running while performing these types of operations. This is done by creating a Pod-DisruptionBudget resource.

> Even though the name of the resource sounds complex, it’s one of the simplest Kubernetes resources available. It contains only a pod label selector and a number specifying the minimum number of pods that must always be available or, starting from Kubernetes version 1.7, the maximum number of pods that can be unavailable.

> As long as it exists, both the Cluster Autoscaler and the kubectl drain command will adhere to it and will never evict a pod with the app=kubia label if that would bring the number of such pods below three.

## Chapter 16 -

> A pod can only be scheduled to a node if it toler-ates the node’s taints.

> This is somewhat different from using node selectors and node affinity, which you’ll learn about later in this chapter. Node selectors and node affinity rules make it possible to select which nodes a pod can or can’t be scheduled to by specifically adding that information to the pod, whereas taints allow rejecting deployment of pods to certain nodes by only adding taints to the node without having to modify existing pods. Pods that you want deployed on a tainted node need to opt in to use the node, whereas with node selectors, pods explicitly specify which node(s) they want to be deployed to.

> Imagine having a single Kubernetes cluster where you run both production and non-production workloads. It’s of the utmost importance that non-production pods never run on the production nodes. This can be achieved by adding a taint to your produc-tion nodes.

> These two tolerations say that this pod tolerates a node being notReady or unreach-able for 300 seconds. The Kubernetes Control Plane, when it detects that a node is no longer ready or no longer reachable, will wait for 300 seconds before it deletes the pod and reschedules it to another node.
 These two tolerations are automatically added to pods that don’t define them. If that five-minute delay is too long for your pods, you can make the delay shorter by adding those two tolerations to the pod’s spec.

> The initial node affinity mechanism in early versions of Kubernetes was the node-Selector field in the pod specification. The node had to include all the labels speci-fied in that field to be eligible to become the target for the pod. 
 Node selectors get the job done and are simple, but they don’t offer everything that you may need. Because of that, a more powerful mechanism was introduced.

> The biggest benefit of the newly introduced node affinity feature is the ability to spec-ify which nodes the Scheduler should prefer when scheduling a specific pod. This is done through the preferredDuringSchedulingIgnoredDuringExecution field.

> You’re defining a node affinity preference, instead of a hard requirement. You want the pods scheduled to nodes that include the labels availability-zone=zone1 and share-type=dedicated. You’re saying that the first preference rule is important by setting its weight to 80, whereas the second one is much less important (weight is set to 20).

> You’ve seen how node affinity rules are used to influence which node a pod is scheduled to. But these rules only affect the affinity between a pod and a node, whereas sometimes you’d like to have the ability to specify the affinity between pods themselves. 
 For example, imagine having a frontend and a backend pod. Having those pods deployed near to each other reduces latency and improves the performance of the app. You could use node affinity rules to ensure both are deployed to the same node, rack, or datacenter, but then you’d have to specify exactly which node, rack, or data-center to schedule them to, which is not the best solution. It’s better to let Kubernetes deploy your pods anywhere it sees fit, while keeping the frontend and backend pods close together. This can be achieved using pod affinity. Let’s learn more about it with an example.

> All the frontend pods were indeed scheduled to the same node as the backend pod.
When scheduling the frontend pod, the Scheduler first found all the pods that match the labelSelector defined in the frontend pod’s podAffinity configuration and then scheduled the frontend pod to the same node.

> You’ve seen how to tell the Scheduler to co-locate pods, but sometimes you may want the exact opposite. You may want to keep pods away from each other. This is called pod anti-affinity. It’s specified the same way as pod affinity, except that you use the podAntiAffinity property instead of podAffinity, which results in the Scheduler never choosing nodes where pods matching the podAntiAffinity’s label selector are running

## Chapter 17 -

> We’ve said that pods can be compared to VMs dedicated to running only a single application. Although an application running inside a pod is not unlike an application running in a VM, significant differences do exist. One example is that apps running in a pod can be killed any time, because Kubernetes needs to relocate the pod to another node for a reason or because of a scale-down request.

> We’ve learned that stateful apps can be run through a StatefulSet, which ensures that when the app starts up on a new node after being rescheduled, it will still see the same host name and persistent state as before. The pod’s IP will change nevertheless. Apps need to be prepared for that to happen.

> If a pod’s container keeps crashing, the Kubelet will keep restarting it indefinitely.
The time between restarts will be increased exponentially until it reaches five minutes.
During those five minute intervals, the pod is essentially dead, because its container’s process isn’t running. To be fair, if it’s a multi-container pod, certain containers may be running normally, so the pod is only partially dead. But if a pod contains only a sin-gle container, the pod is effectively dead and completely useless, because no process is running in it anymore.

> In addition to regular containers, pods can also include init containers. As the name suggests, they can be used to initialize the pod—this often means writing data to the pod’s volumes, which are then mounted into the pod’s main container(s).

> A pod may have any number of init containers. They’re executed sequentially and only after the last one completes are the pod’s main containers started. This means init containers can also be used to delay the start of the pod’s main container(s)—for example, until a certain precondition is met. An init container could wait for a service required by the pod’s main container to be up and ready. When it is, the init container terminates and allows the main container(s) to be started. This way, the main con-tainer wouldn’t use the service before it’s ready.

> lifecycle hooks are specified per container, unlike init containers, which apply to the whole pod. As their names suggest, they’re executed when the container starts and before it stops. 
 Lifecycle hooks are similar to liveness and readiness probes in that they can either  Execute a command inside the container  Perform an HTTP GET request against a URL

> A post-start hook is executed immediately after the container’s main process is started.
You use it to perform additional operations when the application starts. Sure, if you’re the author of the application running in the container, you can always perform those operations inside the application code itself. But when you’re running an application developed by someone else, you mostly don’t want to (or can’t) modify its source code. Post-start hooks allow you to run additional commands without having to touch the app. These may signal to an external listener that the app is starting, or they may initialize the application so it can start doing its job.

> A pre-stop hook is executed immediately before a container is terminated. When a container needs to be terminated, the Kubelet will run the pre-stop hook, if config-ured, and only then send a SIGTERM to the process (and later kill the process if it doesn’t terminate gracefully).

> In contrast to the post-start hook, the container will be terminated regardless of the result of the hook—an error HTTP response code or a non-zero exit code when using a command-based hook will not prevent the container from being terminated.

> Once the Kubelet notices the pod needs to be terminated, it starts terminating each of the pod’s containers. It gives each container time to shut down gracefully, but the time is limited. That time is called the termination grace period and is configu-rable per pod.

> Ensuring each connection is handled properly at pod startup is simple if you under-stand how Services and service Endpoints work. When a pod is started, it’s added as an endpoint to all the Services, whose label selector matches the pod’s labels. As you may remember from chapter 5, the pod also needs to signal to Kubernetes that it’s ready.
Until it is, it won’t become a service endpoint and therefore won’t receive any requests from clients.

> If you don’t specify a readiness probe in your pod spec, the pod is always considered ready. It will start receiving requests almost immediately—as soon as the first kube-proxy updates the iptables rules on its node and the first client pod tries to connect to the service. If your app isn’t ready to accept connections by then, clients will see “connec-tion refused” types of errors.

> You’ll also soon learn that referring to the latest image tag in your pod manifests will cause problems, because you can’t tell which version of the image each individual pod replica is running. Even if initially all your pod replicas run the same image version, if you push a new version of the image under the latest tag, and then pods are resched-uled (or you scale up your Deployment), the new pods will run the new version, whereas the old ones will still be running the old one. Also, using the latest tag makes it impossible to roll back to a previous version (unless you push the old version of the image again).

> Don’t forget to label all your resources, not only Pods. Make sure you add multiple labels to each resource, so they can be selected across each individual dimension. You (or the ops team) will be grateful you did it when the number of resources increases

> If a container crashes and is replaced with a new one, you’ll see the new container’s log. To see the previous container’s logs, use the --previous option with kubectl logs.

## Notes

> How to kubectl exec into container of a multi container pod? Use the --container flag. If omitted, the first container in the pod will be chosen.
